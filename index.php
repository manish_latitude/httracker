<!DOCTYPE html>
<html>
<head>
	<!-- https://www.youtube.com/watch?v=iKlWaUszxB4 -->
	<title>Httracker Login</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" type="text/css" href="css/fb_g.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<script src=""></script>
</head>
<body class="hold-transition login-page">
	<div id="login-div" class="main-div">
		<div class="login-box">
			<div class="login-logo">
				HTTracker
			</div>
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to HTTracker</p>

				<div class="form-group has-feedback">
			        <input type="email" class="form-control" id="userEmail" placeholder="Email.......">
			        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			    </div>
			    <div class="form-group has-feedback">
			        <input type="password"  class="form-control" placeholder="Password......." id="userPassword">
			        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			    </div>
			    <div id="result"></div>
			    <div class="row" align="center">
			        <!-- <div class="col-xs-4"> -->
			         	<button onclick="Login()" class="btn btn-primary btn-block btn-flat" style="width: 30%;">Sign In</button>
			          	<button class="loginBtn loginBtn--facebook" onclick="FbLogin()">Login with Facebook</button>
			          	<button class="loginBtn loginBtn--google" onclick="GLogin()">Login with Google</button>
					<!-- </div> -->
		      	</div>
			</div>
		</div>
	</div>	
</body>
<script src="bower_components/jquery/jquery.min.js"></script>
<script src="js/config.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.1/firebase.js"></script>
<script>
	firebase.initializeApp(config);
</script>
<script type="text/javascript" src="index.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/demo.js"></script>
</html>
		