firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // window.location = 'home.php';
    // $('#user-div').show();
    // $('#login-div').hide();
    var uid = firebase.auth().currentUser.uid;
    setCookie('username',uid,1);
    window.location = 'home.php';
    
  } else {
    // No user is signed in.
    $('#user-div').hide();
    $('#login-div').show();
  }
});

function Login() {
	var email = $('#userEmail').val();
	var password = $('#userPassword').val();

	firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
	  	// Handle Errors here.
	  	var errorCode = error.code;
	  	var errorMessage = error.message;

	  	alert("Error :"+ errorMessage);

	  	// ...
	});
}

var provider;
function FbLogin() {
  provider = new firebase.auth.FacebookAuthProvider();
  sociallogin();
}

function GLogin(){
  provider = new firebase.auth.GoogleAuthProvider();
  sociallogin();
}

function sociallogin(){
  firebase.auth().signInWithPopup(provider).then(function(result){
    var token = result.credential.accessToken;
    var user = result.user;
    console.log(token);
    console.log(user);
  }).catch(function(error){
    console.log(error.code);
    console.log(error.message);
  });
}

function setCookie(cname,cvalue,exdays) {
  // alert('hi');
  var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    // alert(cname + "=" + cvalue + ";" + expires + ";path=/");  
}