<!DOCTYPE html>
<html>
<head>
	<title>Httraker Home</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<script src=""></script>
</head>
<body onload="checkCookie()">
	<div class="wrapper">
		<header class="main-header">
			<span class="logo-lg">
				<center><h3>HTTracker</h3></center>
			</span>
			<div class="navbar-custom-menu">
				<button onclick="humidity()" class="btn btn-primary">Humidity</button>
				<button onclick="temperature()" class="btn btn-primary">Temperature</button>
				<button onclick="Logout()" class="btn btn-primary">Logout</button>
			</div>
		</header>
		<br>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<!-- <div class="box box-success"> -->
						<center>
	            			<div id="monitor-chart" style="height: 250px; width: 70%"></div>
	            		</center>
				    <!-- </div> -->
				</div>
			</div>
		</section>
	</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://www.gstatic.com/charts/loader.js"></script>
<script src="js/config.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.1/firebase.js"></script>
<script>
	firebase.initializeApp(config);

	function getCookie(cname) {
	    var name = cname + "=";
	    var decodedCookie = decodeURIComponent(document.cookie);
	    var decodedCookie = document.cookie;
	    var ca = decodedCookie.split(';');
	    for(var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	}

	var userC;
	function checkCookie() {
	    userC=getCookie("username");
	    if (userC != "") {
	        // alert("Welcome again");
	    } else {
	       // userC = prompt("Please enter your name:","");
	       // if (userC != "" && userC != null) {
	       //     setCookie("username", userC, 30);
	       // }
	       window.location = 'index.php';
	    }
	}

  	function Logout()
	{
		firebase.auth().signOut();
		window.location = "index.php";
	}
	
    humidity();
	function humidity()
	{
		var fb = firebase.database().ref();
		// var cuser = firebase.auth().currentUser.uid;
		userC=getCookie("username");
		var refI = fb.child('Instruments/'+userC);
		var timestamp,d;
		var data = new Array();
		refI.on('value',function(snapshot){
	    	snapshot.forEach(function(snap){
	    		var refS;
	    		if(snap.val().Tracker_ID){
	    			refS = fb.child('SensorData/'+userC+'/'+snap.val().Tracker_ID);
	    		}else{
	    			refS = fb.child('SensorData/'+userC+'/'+snap.val().TrackerID);
	    		}
	    		refS.on('value',function(snapshotS){
	    			snapshotS.forEach(function(snapS){
	    				timestamp = new Date();
	    				temp = snapS.val().Humidity;
	    				data[humidity] = parseInt(temp);
	    			});
	    			d = Object.values(data);
		        	$(function(){
		        		google.charts.load('current', {
						  callback: function () {
						    var chart = new google.visualization.LineChart(document.getElementById('monitor-chart'));

						    var options = {'title' : 'Humidity',
						      animation: {
						        duration: 1000,
						        easing: 'out',
						        startup: true
						      },
						      hAxis: {
						        title: 'Time'
						      },
						      vAxis: {
						        title: 'Humidity'
						      },
						    };

						    var data = new google.visualization.DataTable();
						    data.addColumn('datetime', 'Time');
						    data.addColumn('number', 'Humidity');

						    // var formatDate = new google.visualization.DateFormat({pattern: 'hh:mm'});
						    // var formatNumber = new google.visualization.NumberFormat({pattern: '#,##0.0'});

						    getTemp();
						    setInterval(getTemp, 5000);
						    function getTemp() {
						      var timestamp = new Date();
						      drawChart(timestamp, temp);
						    }


						    function drawChart(timestamp, temp) {

						    	if(d < 0){
						    		var num = Math.abs(d);
						    	}else{
						    		var num = d;
						    	}

						      	data.addRow([timestamp, num]);

						      // formatDate.format(data, 0);
						      // formatNumber.format(data, 1);

						      chart.draw(data, options);
						    }
						  },
						  packages:['corechart']
						});
		        	})	
				});	
	    	});
	    });
	}

	function temperature()
	{
		var fb = firebase.database().ref();
		// var cuser = firebase.auth().currentUser.uid;
		userC=getCookie("username");
		var refI = fb.child('Instruments/'+userC);
		var timestamp,d;
		var data = new Array();
		refI.on('value',function(snapshot){
	    	snapshot.forEach(function(snap){
	    		var refS;
	    		if(snap.val().Tracker_ID){
	    			refS = fb.child('SensorData/'+userC+'/'+snap.val().Tracker_ID);
	    		}else{
	    			refS = fb.child('SensorData/'+userC+'/'+snap.val().TrackerID);
	    		}
	    		refS.on('value',function(snapshotS){
	    			snapshotS.forEach(function(snapS){
	    				timestamp = new Date();
	    				temp = snapS.val().Temperature;
	    				data[temp] = parseInt(temp);
	    			});
	    			d = Object.values(data);
		        	$(function(){
		        		google.charts.load('current', {
						  callback: function () {
						    var chart = new google.visualization.LineChart(document.getElementById('monitor-chart'));

						    var options = {'title' : 'Temperature',
						      animation: {
						        duration: 1000,
						        easing: 'out',
						        startup: true
						      },
						      hAxis: {
						        title: 'Time'
						      },
						      vAxis: {
						        title: 'Temperature'
						      },
						    };

						    var data = new google.visualization.DataTable();
						    data.addColumn('datetime', 'Time');
						    data.addColumn('number', 'Temperature');

						    // var formatDate = new google.visualization.DateFormat({pattern: 'hh:mm'});
						    // var formatNumber = new google.visualization.NumberFormat({pattern: '#,##0.0'});

						    getTemp();
						    setInterval(getTemp, 5000);
						    function getTemp() {
						      var timestamp = new Date();
						      drawChart(timestamp, temp);
						    }


						    function drawChart(timestamp, temp) {

						    	if(d < 0){
						    		var num = Math.abs(d);
						    	}else{
						    		var num = d;
						    	}

						      	data.addRow([timestamp, parseInt(d)]);

						      // formatDate.format(data, 0);
						      // formatNumber.format(data, 1);

						      chart.draw(data, options);
						    }
						  },
						  packages:['corechart']
						});
		        	})	
				});	
	    	});
	    });
	}
</script>
<!-- <script type="text/javascript" src="index.js"></script> -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/demo.js"></script>
</html>